<?php
declare(strict_types=1);

namespace StepanDalecky\KmlParser\Entities;

class Coordinates extends Entity
{

	public function getCoordinates(): object
	{
		return $this->element->getAttribute('Coordinates');
	}

	public function getValue(): string
	{
		return $this->element->getValue();
	}


}
