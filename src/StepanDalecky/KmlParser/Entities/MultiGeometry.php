<?php
declare(strict_types=1);

namespace StepanDalecky\KmlParser\Entities;

class MultiGeometry extends Entity
{

	public function getMultiGeoMetry(): string
	{
		return $this->element->getAttribute('MultiGeometry');
	}

	public function getValue(): string
	{
		return $this->element->getValue();
	}

	public function getPolygon(): Polygon
	{
		return new Polygon($this->element->getChild('Polygon'));
	}
}
